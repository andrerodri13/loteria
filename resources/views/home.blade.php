@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if ($errors->has('referencia'))
                            <span class="text-danger">
                                <strong>{{ mb_strtoupper($errors->first('referencia')) }}</strong>
                            </span>
                        @endif

                        {!! Form::open(['route' => 'cadastrar']) !!}

                        <div class="form-group">
                            {!! Form::label('dezenas', 'Dezenas', ['class' => 'control-label']) !!}
                            {!! Form::text('dezenas', $dezenas, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('referencia', 'Referência', ['class' => 'control-label']) !!}
                            {!! Form::text('referencia', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form group">
                            {!! Form::button('Gerar', ['type' => 'button', 'onclick' => 'location.reload()', 'class' => 'btn btn-primary']); !!}
                            {!! Form::submit('Salvar', ['class' => 'btn btn-success']); !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
