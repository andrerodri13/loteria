<?php

namespace App\Http\Controllers;

use App\aposta;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * @var aposta
     */
    private $aposta;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(aposta $aposta)
    {

        $this->aposta = $aposta;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function gerar()
    {
        for ($i = 1; $i <= 6; $i++) {
            $n[] = str_pad(rand(1, 60), 2, '0', STR_PAD_LEFT);
        }
        sort($n);
        $dezenas = implode('-', $n);
        return view('home', compact('dezenas'));
    }

    public function cadastrar(Request $request)
    {
        $validatedData = $request->validate([
            'referencia' => 'required',
        ]);

        $this->aposta->create($request->all());
        return redirect()->route('gerar');
    }
}
