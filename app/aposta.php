<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class aposta extends Model
{
    protected $fillable = ['dezenas', 'referencia'];
}
